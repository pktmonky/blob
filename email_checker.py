#!/usr/bin/python
from sys import argv, stderr, exit
from re import compile
import socket

# Taken from https://github.com/noelbush/py_email_validation
# RFC 2822 - style email validation for Python
# (c) 2011 Noel Bush <noel@aitools.org>
# This code is made available to you under the GNU LGPL v3.
WSP = r'[\s]'                                        # see 2.2.2. Structured Header Field Bodies
CRLF = r'(?:\r\n)'                                   # see 2.2.3. Long Header Fields
NO_WS_CTL = r'\x01-\x08\x0b\x0c\x0f-\x1f\x7f'        # see 3.2.1. Primitive Tokens
QUOTED_PAIR = r'(?:\\.)'                             # see 3.2.2. Quoted characters
FWS = r'(?:(?:' + WSP + r'*' + CRLF + r')?' + \
      WSP + r'+)'                                    # see 3.2.3. Folding white space and comments
CTEXT = r'[' + NO_WS_CTL + \
        r'\x21-\x27\x2a-\x5b\x5d-\x7e]'              # see 3.2.3
CCONTENT = r'(?:' + CTEXT + r'|' + \
           QUOTED_PAIR + r')'                        # see 3.2.3 (NB: The RFC includes COMMENT here
# as well, but that would be circular.)
COMMENT = r'\((?:' + FWS + r'?' + CCONTENT + \
          r')*' + FWS + r'?\)'                       # see 3.2.3
CFWS = r'(?:' + FWS + r'?' + COMMENT + ')*(?:' + \
       FWS + '?' + COMMENT + '|' + FWS + ')'         # see 3.2.3
ATEXT = r'[\w!#$%&\'\*\+\-/=\?\^`\{\|\}~]'           # see 3.2.4. Atom
ATOM = CFWS + r'?' + ATEXT + r'+' + CFWS + r'?'      # see 3.2.4
DOT_ATOM_TEXT = ATEXT + r'+(?:\.' + ATEXT + r'+)*'   # see 3.2.4
DOT_ATOM = CFWS + r'?' + DOT_ATOM_TEXT + CFWS + r'?' # see 3.2.4
QTEXT = r'[' + NO_WS_CTL + \
        r'\x21\x23-\x5b\x5d-\x7e]'                   # see 3.2.5. Quoted strings
QCONTENT = r'(?:' + QTEXT + r'|' + \
           QUOTED_PAIR + r')'                        # see 3.2.5
QUOTED_STRING = CFWS + r'?' + r'"(?:' + FWS + \
                r'?' + QCONTENT + r')*' + FWS + \
                r'?' + r'"' + CFWS + r'?'
LOCAL_PART = r'(?:' + DOT_ATOM + r'|' + \
             QUOTED_STRING + r')'                    # see 3.4.1. Addr-spec specification
DTEXT = r'[' + NO_WS_CTL + r'\x21-\x5a\x5e-\x7e]'    # see 3.4.1
DCONTENT = r'(?:' + DTEXT + r'|' + \
           QUOTED_PAIR + r')'                        # see 3.4.1
DOMAIN_LITERAL = CFWS + r'?' + r'\[' + \
                 r'(?:' + FWS + r'?' + DCONTENT + \
                 r')*' + FWS + r'?\]' + CFWS + r'?'  # see 3.4.1
DOMAIN = r'(?:' + DOT_ATOM + r'|' + \
         DOMAIN_LITERAL + r')'                       # see 3.4.1
ADDR_SPEC = r'(' + LOCAL_PART + r')@(' + DOMAIN + \
            r')'                                     # see 3.4.1
VALID_ADDRESS_REGEXP = '^' + ADDR_SPEC + '$'

# Code snippet taken from
# http://www.binarytides.com/python-program-to-fetch-domain-whois-data-using-sockets/
def perform_whois(server , query) :
    # socket connection
    s = socket.socket(socket.AF_INET , socket.SOCK_STREAM)
    s.connect((server , 43))
    s.send(query + '\r\n')
    # get response
    msg = ''
    while len(msg) < 10000:
        chunk = s.recv(100)
        if(chunk == ''):
            break
        msg = msg + chunk
    return msg

# Code snippet taken from
# http://www.binarytides.com/python-program-to-fetch-domain-whois-data-using-sockets/
def get_whois_server(ext):
    whois = 'whois.iana.org'
    msg = perform_whois(whois , ext)
    for line in msg.splitlines():
        if ':' in line:
            key_value = line.split(':')
            if key_value[0] == 'whois':
                return key_value[1].replace(' ', '')
    return False

# Use "Registrar IANA ID" for validating domain
def registrar_id(server, query):
    msg = perform_whois(server, query)
    for line in msg.splitlines():
        if ':' in line:
            key_value = line.split(':')
            if "Registrar IANA ID" in key_value[0]:
                return key_value[1]
    return False

# Check email against regex and lengths
def test_email(email):
    test = compile(VALID_ADDRESS_REGEXP)
    m = test.match(email)
    if m is not None:
        if len(m.group(1)) > 64:
            return False
        if len(m.group(2)) > 255:
            return False
        return (m.group(1), m.group(2))
    else:
        return False

if __name__ == "__main__":
    # Test command line inputs
    if len(argv) == 1 or len(argv) > 2:
        exit("Usage: %s filename" % argv[0])

    # Open file and save to list 'lines'
    try:
        lines=[]
        with open(argv[1]) as f:
            lines=f.readlines()
    # Otherwise, exit the script with error message
    except:
        exit("File \"%s\" does not exist" % file)

    # Set up error log file
    error=open('email_checker.log', 'wb')
    # set()s for output
    registered=set()
    unregistered=set()
    bad_tld=set()
    bad_email=set()
    for line in lines:
        email_address = line.rstrip()
        # Is the email in RFC5322 format
        email_parts = test_email(email_address)
        if email_parts:
            domain = email_parts[1]
            ext = domain.split('.')[-1]
            # Check if WhoIS server exists for TLD
            whois = get_whois_server(ext)
            if whois:
                # Check if Registrar IANA ID exists for WhoIS record
                valid_registrar = registrar_id(whois, domain)
                if valid_registrar:
                    registered.add(domain)
                else:
                    unregistered.add(domain)
            else:
                bad_tld.add(domain)
        else:
            bad_email.add(email_address)
    error.close()

    # Send report to stdout
    print "\nList of unique domains in %s" % argv[1]
    for domain in registered:
        print "\t%s" % domain

    print "\nList of unregistered domains in %s" % argv[1]
    for domain in unregistered:
        print "\t%s" % domain

    print "\nList of domains with no TLD or a nonexistant TLD in %s" % argv[1]
    for domain in bad_tld:
        print "\t%s" % domain

    print "\nList of bad email addresses in %s" % argv[1]
    for email in bad_email:
        print "\t%s" % email
